import { Router } from '@kominal/lib-node-express-router';
import { TranslationDatabase } from '../models/translation';

export const projectRouter = new Router();

projectRouter.getAsUser<{ tenantId: string }, string[], never>(
	'/tenants/:tenantId/projects',
	async (req) => {
		const projects = await TranslationDatabase.aggregate([
			{ $match: { tenantId: req.params.tenantId } },
			{ $group: { _id: '$projectName' } },
		]);

		return {
			statusCode: 200,
			responseBody: projects.map((p) => p._id),
		};
	},
	{
		permissions: ['core.user-service.read', 'core.user-service.write'],
	}
);
